/*
 * bad_format.hpp
 *
 *  Created on: Jul 2, 2017
 *      Author: calegria
 */

#ifndef BS_BAD_FORMAT_HPP_
#define BS_BAD_FORMAT_HPP_

#include <stdexcept>
#include <string>

namespace bs
{
  class bad_format : public std::runtime_error
  {
  public:

    bad_format (const char *message) : runtime_error(message)
    {
    }

    bad_format (const std::string &message) : runtime_error (message)
    {
    }

    bad_format (const bad_format &ex) : runtime_error (ex)
    {
    }

    virtual
    ~bad_format ()
    {
    }
  };

} /* namespace bs */

#endif /* BS_BAD_FORMAT_HPP_ */
