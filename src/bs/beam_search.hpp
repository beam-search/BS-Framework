/*
 * BeamSearch.hpp
 *
 *  Created on: May 30, 2017
 *      Author: calegria
 */

#ifndef BS_BEAM_SEARCH_HPP_
#define BS_BEAM_SEARCH_HPP_

#include <iostream>
#include <string>
using namespace std;

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/properties.hpp>

#include "iterator.hpp"

// property type for bs nodes
//
struct bs_node_t {
    typedef vertex_property_tag kind;
};

namespace bs
{
//
// data members
//
// typedef NODE node_type
//
// functions
//
// unsigned int getFilterWidth() const;
// unsigned int getBeamWidth() const;
// void filter_nodes(Iterator in_begin, Iterator in_end, Iterator out_begin, Iterator out_end) const;
// bool stop_condition();
// Node getRootNode();
//
  template<class BSFilter>
    void
    beam_search (BSFilter &filter)
    {
#ifdef DEBUG
      cout << "filter width = " << filter.getFilterWidth () << std::endl;
      cout << "beam width = " << filter.getBeamWidth () << std::endl;
#endif

      //
      // data types
      //

      typedef typename BSFilter::node_type Node;
      typedef property<bs_node_t, Node> BSNodeProperty;
      typedef adjacency_list<listS, vecS, directedS, BSNodeProperty> Graph;
      typedef typename graph_traits<Graph>::vertex_descriptor Vertex;
      typedef typename property_map<Graph, bs_node_t>::type PropertyMap;


      //
      // tree initialization
      //

      Graph bs_tree;
//      PropertyMap bs_map = get (bs_node_t (), bs_tree);

      // root vertex
      //
//      Vertex root_v = add_vertex (bs_tree);
//      bs_map[root_v] = null;

      // array of leaf vertices
      //
      const size_t beam_front_size = filter.get_beam_width ();
      const size_t filter_front_size = filter.get_filter_width ();
      const size_t front_size = beam_front_size * filter_front_size;

      Node **beam_front = new Node*[beam_front_size];
      Node **filter_front = new Node*[front_size];

      for (size_t i = 0; i < beam_front_size; i++) beam_front[i] = nullptr;
      for (size_t i = 0; i < front_size; i++) filter_front[i] = nullptr;


      //
      // main cycle
      //

//      BSIterator<Graph, PropertyMap> bs_it_begin (bs_tree, bs_map, root_v);
//      BSIterator<Graph, PropertyMap> bs_it_end (bs_tree, bs_map, root_v);

      // filter nodes for the first level
      //
      filter.filter_nodes (filter.get_root_node (), beam_front,
			   beam_front + filter_front_size);

#ifdef NDEBUG
//      cout << "On beam_search.hpp - after initial filter node" << endl;
//      for (size_t i = 0; i < filter_front_size; i++)
//	{
//	  cout << i << " = " << *filter_front[i] << endl;
//	  cout << endl;
//	}
//      cout << string (10, '-') << endl;
#endif

//      while (filter.is_stop())
//	{
//	  // filter nodes
//	  //
//	  for (auto f_it = beam_front, s_it = filter_front;
//	      f_it != beam_front + beam_front_size; ++f_it, s_it +=
//		  filter_front_size)
//	    {
//	      if (f_it == nullptr) continue;
//	      bs_it_begin.setCurrentVertex (*f_it);
//	      filter.filter_nodes (bs_it_begin, bs_it_end, s_it,
//				   s_it + filter_front_size);
//	    }
//
//#ifdef NDEBUG
//	  cout << "On beam_search.hpp - filtered nodes" << endl;
//	  for (size_t i = 0; i < front_size; i++)
//	    {
//	      cout << i << " = " << *filter_front << endl;
//	      cout << endl;
//	    }
//	  cout << string (10, '-') << endl;
//#endif
//	}

//      cout << "On beam_search.hpp front" << endl;
//      for (auto node = filter_front; node != filter_front + front_size; ++node)
//	{
//	  cout << "Property One = " << (*node)->getPropertyOne () << endl;
//	  cout << "Property Two = " << (*node)->getPropertyTwo () << endl;
//	  cout << endl;
//
//	  Vertex v = add_vertex (bs_tree);
//	  add_edge (root_v, v, bs_tree);
//	  bs_map[v] = (*node);
//	}
//      cout << string (10, '-') << endl;
//
//      cout << "On beam_search.hpp graph" << endl;
//      typename graph_traits<Graph>::vertex_iterator first, last;
//      for (tie (first, last) = vertices (bs_tree); first != last; ++first)
//	{
//	  Node *node = bs_map[*first];
//	  cout << "Property One = " << node->getPropertyOne () << endl;
//	  cout << "Property Two = " << node->getPropertyTwo () << endl;
//	  cout << endl;
//	}

      //
      // clean up
      //

      delete[] beam_front;
      delete[] filter_front;
    }
}

#endif /* BS_BEAM_SEARCH_HPP_ */
