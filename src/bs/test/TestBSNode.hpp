/*
 * TestBSNode.h
 *
 *  Created on: Jun 15, 2017
 *      Author: calegria
 */

#ifndef BS_TEST_TESTBSNODE_HPP_
#define BS_TEST_TESTBSNODE_HPP_

namespace bs
{

  class TestBSNode
  {
  public:
    TestBSNode (int p1 = 2, int p2 = 3) :
	property_one (p1), property_two (p2)
    {
    }

    virtual
    ~TestBSNode ();

    inline int
    getPropertyOne () const
    {
      return property_one;
    }

    inline int
    getPropertyTwo () const
    {
      return property_two;
    }

  private:
    int property_one;
    int property_two;
  };

} /* namespace bs */

#endif /* BS_TEST_TESTBSNODE_HPP_ */
