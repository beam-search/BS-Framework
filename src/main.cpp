/*
 * main.cpp
 *
 *  Created on: May 27, 2017
 *      Author: calegria
 */

#include <boost/program_options.hpp>
namespace po = boost::program_options;

#include <type_traits>

#include <algorithm>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
#include <vector>
using namespace std;

#include "bs/2DBPPGC_irregular_pieces/homogeneous_bins_bs.hpp"
#include "bs/beam_search.hpp"

// the help command-line option
//
#define OP_HELP "help"
#define OPS_HELP "h"

// the bin length and width command-line option, length >= width
//
#define OP_LENGTH "bin-length"
#define OPS_LENGTH "l"
#define OP_WIDTH "bin-width"
#define OPS_WIDTH "w"

// the filter width and beam width parameters for the beam search algorithm
//
#define OP_BIN_WIDTH "filter-width"
#define OPS_BIN_WIDTH "f"
#define OPD_FILTER_WIDTH 2
#define OP_BEAM_WIDTH "beam-width"
#define OPS_BIN_HEIGHT "b"
#define OPD_BEAM_WIDTH 3

// the name of the file containing the pieces information
//
#define OP_DATA "data"

//
//
int
main (int argc, const char **argv)
{
  //
  // reading command line options
  //

  bs::Polygon::FT length;
  bs::Polygon::FT width;
  size_t filter_width;
  size_t beam_width;
  string file_name;

  po::options_description desc ("Allowed options");
  desc.add_options ()
      (OP_HELP "," OPS_HELP, "Print this help message")
      (OP_LENGTH "," OPS_LENGTH, po::value<bs::Polygon::FT>(&length),
	  "The length of the bins")
      (OP_WIDTH "," OPS_WIDTH, po::value<bs::Polygon::FT>(&width),
	  "The width of the bins")
      (OP_BIN_WIDTH "," OPS_BIN_WIDTH,
	  po::value<size_t>(&filter_width)->default_value(OPD_FILTER_WIDTH),
	  "The filter width of the beam search algorithm")
      (OP_BEAM_WIDTH "," OPS_BIN_HEIGHT,
	  po::value<size_t>(&beam_width)->default_value(OPD_BEAM_WIDTH),
	  "The beam width of the beam search algorithm")
      (OP_DATA, po::value<string> (&file_name),
	  "The *.dat name of the file containing the input pieces information");

  po::positional_options_description p_desc;
  p_desc.add (OP_DATA, -1);

  po::variables_map vm;
  po::store (
      po::command_line_parser (argc, argv).options (desc).positional (p_desc).run (),
      vm);
  po::notify (vm);


  //
  // operating on options
  //

  // help
  //
  if (vm.count (OP_HELP))
    {
      cout << desc << endl;
      exit (EXIT_SUCCESS);
    }

  // bin length
  //
  if (!vm.count(OP_LENGTH))
    {
      cout << desc << endl;
      exit (EXIT_SUCCESS);
    }

  // bin width
  //
  if (!vm.count(OP_WIDTH))
    {
      cout << desc << endl;
      exit (EXIT_SUCCESS);
    }

  // data
  //
  vector<bs::Piece *> pieces;
  if (vm.count (OP_DATA))
    {
      // reading pieces from stream
      //
      ifstream in (vm[OP_DATA].as<string> (), ifstream::in);

      bs::read_pieces (pieces, in);
      in.close();

      // all our algorithms are based in the pieces being sorted by area in
      // non-increasing order
      //
      std::sort (pieces.begin (), pieces.end (),
		 [] (const bs::Piece *a, const bs::Piece *b)
		   { return *a > *b;});
    }
  else
    {
      cout << desc << endl;
      exit (EXIT_SUCCESS);
    }

#ifdef NDEBUG
  cout << std::string(10, '-') << endl;
  cout << "options" << endl;
  cout << std::string(10, '-') << endl;
  cout << OP_WIDTH " = " << width << endl;
  cout << OP_LENGTH " = " << length << endl;
  cout << OP_BIN_WIDTH " = " << filter_width << endl;
  cout << OP_BEAM_WIDTH " = " << beam_width << endl;
  cout << OP_DATA << " = " << file_name << endl;
  cout << std::string(10, '-') << endl;
  cout << "pieces" << endl;
  cout << std::string(10, '-') << endl;
  std::for_each (pieces.begin (), pieces.end (), [] (const bs::Piece *p)
    { cout << *p << endl;});
  cout << std::string(10, '-') << endl;
#endif


  //
  // processing data
  //

  bs::homogeneous_bins_bs hbb(pieces, width, length, filter_width, beam_width);

#ifdef NDEBUG
  cout << "homogeneous bins beam search" << endl;
  cout << std::string(10, '-') << endl;
  cout << hbb;
  cout << std::string(10, '-') << endl;
#endif

  bs::beam_search (hbb);
}
