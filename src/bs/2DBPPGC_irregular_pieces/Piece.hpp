/*
 * Piece.hpp
 *
 *  Created on: Jun 30, 2017
 *      Author: calegria
 */

#ifndef PIECE_HPP_
#define PIECE_HPP_

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Aff_transformation_2.h>
#include <CGAL/Polygon_2.h>
#include <CGAL/Point_2.h>

#include <string>
#include <iostream>
#include <iterator>
#include <vector>

#include "bad_format.hpp"

namespace bs
{
  typedef CGAL::Exact_predicates_inexact_constructions_kernel Kernel;
  typedef CGAL::Aff_transformation_2<Kernel::RT> Transformation;
  typedef CGAL::Polygon_2<Kernel> Polygon;
  typedef CGAL::Point_2<Kernel> Point;

  //
  //
  //
  class Piece : public Polygon
  {
    inline friend std::ostream &
    operator<< (std::ostream &os, const Piece &p)
    {
      os << "id = " << p.id << ", area = " << p.area_value << ", "
	  << (Polygon &) p;
      return os;
    }
    inline friend bool
    operator> (const Piece &a, const Piece &b)
    {
      CGAL::Comparison_result res = CGAL::compare (a.area_value, b.area_value);
      return res == CGAL::LARGER;
    }

  public:

    template<class InputIterator>
      Piece (std::size_t id, InputIterator first, InputIterator last) :
	  Polygon (first, last), id (id), area_value (Polygon::area ()),
	  y_symmetric (nullptr)
      {
      }

    virtual
    ~Piece ()
    {
    }

    unsigned int
    get_id () const
    {
      return this->id;
    }

    Kernel::FT
    area () const
    {
      return this->area_value;
    }

    bool is_y_symmetric()
    {
      if (y_symmetric == nullptr)
	{
	  return *y_symmetric;
	}

      Transformation y_reflection(-1, 0, 0, 0, 1, 0, 1);
      y_symmetric = new bool;

      //
      // reflects and translates polygon to get both right-most vertices with
      // the same x-coordinate
      //

      return false;
    }

  private:

    const Kernel::FT area_value;
    const std::size_t id;
    bool *y_symmetric;
  };

  //
  //
  //
  void
  read_line (std::istream &is, std::size_t &poly_no, unsigned int &point_no,
	     int &x, int &y)
  {

    // polyNo
    //
    if ((is >> poly_no).fail ())
      {
	throw bad_format ("Expected unsigned integer (polyNo)");
      }

    if (poly_no == 0)
      {
	throw bad_format ("Field polyNo must be greater than zero");
      }

    // pointNo
    //
    if (!is || (is >> point_no).fail ())
      {
	throw bad_format ("Expected unsigned integer (pointNo)");
      }

    if (point_no == 0)
      {
	throw bad_format ("Field pointNo must be greater than zero");
      }

    // X
    //
    if (!is || (is >> x).fail ())
      {
	throw bad_format ("Expected integer (X)");
      }

    // Y
    //
    if (!is || (is >> y).fail ())
      {
	throw bad_format ("Expected integer (Y)");
      }
  }

  //
  //
  //
  void
  read_pieces (std::vector<Piece *> &container, std::istream &is)
  {
    if (!is)
      {
	throw std::invalid_argument ("input stream with no input content");
      }

      {
	std::string skip;
	is >> skip >> skip >> skip >> skip;
      }

    std::vector<Point> v;
    std::size_t poly_no;
    std::size_t poly_no_bck;
    unsigned int point_no;
    int x;
    int y;

#ifdef NDEBUG
    int line_no = 1;
#endif

    try
      {
	read_line (is, poly_no_bck, point_no, x, y);
	while (!is.eof ())
	  {
#ifdef NDEBUG
	    line_no++;
#endif
	    v.push_back (Point (x, y));
	    read_line (is, poly_no, point_no, x, y);
	    if (poly_no_bck != poly_no)
	      {
		container.push_back (new Piece (poly_no_bck, v.begin (), v.end ()));
		poly_no_bck = poly_no;
		v.clear ();
	      }
	  }
	v.push_back (Point (x, y));
	container.push_back (new Piece (poly_no, v.begin (), v.end ()));
      }
    catch (std::exception &)
      {
#ifdef NDEBUG
	std::cerr << "Exception on line " << line_no << std::endl;
#endif
	throw;
      }
  }

} /* namespace bs */

#endif /* PIECE_HPP_ */
