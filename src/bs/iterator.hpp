/*
 * BSIterator.h
 *
 *  Created on: Jun 23, 2017
 *      Author: calegria
 */

#ifndef BS_ITERATOR_HPP_
#define BS_ITERATOR_HPP_

#include <boost/graph/properties.hpp>
#include <tuple>

using namespace boost;

//
//
//

template<typename Graph, typename PropertyMap>
  class BSIterator
  {
    typedef typename property_traits<PropertyMap>::value_type Node;
    typedef typename graph_traits<Graph>::out_edge_iterator EdgeIterator;
    typedef typename graph_traits<Graph>::vertex_descriptor Vertex;

  public:

    //
    //
    BSIterator (const Graph &g, const PropertyMap &m, Vertex &v) :
	graph (g), property_map (m), current_vertex (v)
    {
    }

    //
    //
    BSIterator (const Graph &g, const PropertyMap &m) :
	graph (g), property_map (m)
    {
      this->current_vertex = NULL;
    }

    virtual
    ~BSIterator ()
    {
    }

    // prefix increment operator
    //
    BSIterator &
    operator++ ()
    {
      increment ();
      return *this;
    }

    // postfix increment operator
    //
    BSIterator
    operator++ (int)
    {
      BSIterator it = *this;
      increment ();
      return it;
    }

    // assignment operator
    //
    const BSIterator &
    operator= (const BSIterator &bs_it)
    {
      this->graph = bs_it.graph;
      this->property_map = bs_it.property_map;
      this->current_vertex = bs_it.current_vertex;
      return *this;
    }

    // equality operator
    //
    bool
    operator== (const BSIterator &bs_it) const
    {
      return this->current_vertex == bs_it.current_vertex;
    }

    // inequality operator; returns opposite of == operator
    //
    inline bool
    operator!= (const BSIterator &right) const
    {
      return !(*this == right);
    }

    Node &
    operator* () const
    {
      return property_map[current_vertex];
    }

    const Vertex&
    get_current_vertex () const
    {
      return current_vertex;
    }

    void
    set_current_vertex (const Vertex& currentVertex)
    {
      current_vertex = currentVertex;
    }

  private:

    const Graph &graph;
    const PropertyMap &property_map;
    Vertex &current_vertex;

    void
    increment ()
    {
      EdgeIterator first, last;
      std::tie (first, last) = out_edges (current_vertex, graph);
      current_vertex = source (*last, graph);
    }
  };

#endif /* BS_ITERATOR_HPP_ */
