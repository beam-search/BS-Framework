/*
 * homogeneousbinsbsnode.hpp
 *
 *  Created on: Jul 10, 2017
 *      Author: calegria
 */

#ifndef BS_2DBPPGC_IRREGULAR_PIECES_HOMOGENEOUS_BIN_HPP_
#define BS_2DBPPGC_IRREGULAR_PIECES_HOMOGENEOUS_BIN_HPP_

#include <vector>

#include "Piece.hpp"

namespace bs
{

  class homogeneous_bin
  {
  public:

    homogeneous_bin (Kernel::FT length, Kernel::FT width,
		     vector<Piece *> &available_pieces) :
	length (length), width (width), area (length * width), waste (area),
	usage (0), global_evaluation (0), open (true), sections (0),
	available_pieces (available_pieces)
    {
    }
    virtual
    ~homogeneous_bin ()
    {
    }

    Kernel::FT
    get_length () const
    {
      return length;
    }

    Kernel::FT
    get_width () const
    {
      return width;
    }

    Kernel::FT
    get_waste () const
    {
      return waste;
    }
    void
    set_waste (Kernel::FT waste)
    {
      assert(CGAL::compare (waste, this->area) != CGAL::LARGER);
      this->waste = waste;
    }

    Kernel::FT
    get_usage () const
    {
      return waste;
    }
    void
    set_usage (Kernel::FT usage)
    {
      assert(CGAL::compare (waste, this->area) != CGAL::LARGER);
      this->usage = usage;
    }

    double
    get_global_evaluation () const
    {
      return global_evaluation;
    }
    void
    set_global_evaluation (double global_evaluation)
    {
      this->global_evaluation = global_evaluation;
    }

    bool
    is_open () const
    {
      return open;
    }
    void
    set_open (bool open)
    {
      this->open = open;
    }

    std::vector<Piece *> &
    get_packed_pieces ()
    {
      return this->packed_pieces;
    }
    const std::vector<Piece *> &
    get_available_pieces () const
    {
      return this->available_pieces;
    }

  private:

    const Kernel::FT length;
    const Kernel::FT width;
    const Kernel::FT area;
    const std::vector<Piece *> available_pieces;

    Kernel::FT waste;
    Kernel::FT usage;
    double global_evaluation;
    unsigned int sections;
    bool open;

    std::vector<Piece *> packed_pieces;
  };

} /* namespace bs */

#endif /* BS_2DBPPGC_IRREGULAR_PIECES_HOMOGENEOUS_BIN_HPP_ */
