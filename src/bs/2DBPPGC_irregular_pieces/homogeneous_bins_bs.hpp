/*
 * homogeneousbinsbs.h
 *
 *  Created on: Jul 6, 2017
 *      Author: calegria
 */

#ifndef BS_2DBPPGC_IRREGULAR_PIECES_HOMOGENEOUS_BINS_BS_HPP_
#define BS_2DBPPGC_IRREGULAR_PIECES_HOMOGENEOUS_BINS_BS_HPP_

#include <numeric>
#include <vector>
#include <algorithm>

#include "homogeneous_bin.hpp"
#include "Piece.hpp"

#define ROTATIONS_NUMBER 3
#define CHILDS_NUMBER 10

namespace bs
{

  class homogeneous_bins_bs
  {
    inline friend std::ostream &
    operator<< (std::ostream &os, const homogeneous_bins_bs &b)
    {
      os << "pieces_area  = " << b.pieces_area << endl
	 << "bin_length   = " << b.bin_length <<  endl
	 << "bin_width    = " << b.bin_width << endl
	 << "filter_width = " << b.filter_width << endl;
      return os;
    }

  public:

    typedef homogeneous_bin node_type;

    homogeneous_bins_bs (vector<Piece *> &pieces_v, Polygon::FT bin_length,
			 Polygon::FT bin_width, size_t filter_width,
			 size_t beam_width) :
	pieces (pieces_v), pieces_area (
	    accumulate (pieces_v.begin (), pieces_v.end (), 0,
			[](Polygon::FT a, const Piece *b)
			  { return a + b->area();})),
	bin_length (bin_length), bin_width (bin_width),
	filter_width (filter_width), beam_width (beam_width),
	root (bin_length, bin_width, pieces_v), stop (false)
    {
    }
    virtual
    ~homogeneous_bins_bs ()
    {
    }

    std::size_t
    get_filter_width () const
    {
      return filter_width;
    }

    std::size_t
    get_beam_width () const
    {
      return beam_width;
    }

    const node_type &
    get_root_node () const
    {
      return root;
    }

    bool
    is_stop () const
    {
      return stop;
    }

    template<class OutIterator>
      void
      filter_nodes (const node_type &father, OutIterator fit_ini,
		    OutIterator fit_end)
      {
	const vector<Piece *> &available_pieces =
	    father.get_available_pieces ();

	// terminate if there are no more pieces available
	//
	if (available_pieces.empty ())
	  {
	    this->stop = true;
	    return;
	  }

	// restrict number of childs if there are few pieces
	//
	if (available_pieces.size () < childs_number)
	  {
	    childs_number = available_pieces.size ();
	  }

	cout << "On TestBS.hpp - input" << endl;

	cout << "father.get_length = " << father.get_length () << endl;
	cout << "father.get_width = " << father.get_width () << endl;
	cout << endl;

//	cout << string (10, '-') << endl;
//
//	cout << "On TestBS.hpp - output" << endl;
//	for (; fit_ini != fit_end; ++fit_ini)
//	  {
//	    TestBSNode *node = new TestBSNode (std::rand (), std::rand ());
//	    cout << "Property One = " << node->getPropertyOne () << endl;
//	    cout << "Property Two = " << node->getPropertyTwo () << endl;
//	    cout << endl;
//
//	    (*fit_ini) = node;
//	  }
//	cout << string (10, '-') << endl;
      }

  private:

    vector<Piece *> &pieces;

    const Polygon::FT pieces_area;
    const Polygon::FT bin_length;
    const Polygon::FT bin_width;

    const std::size_t filter_width;
    const std::size_t beam_width;
    const node_type root;

    unsigned int childs_number = ROTATIONS_NUMBER;

    bool stop;
  };

} /* namespace bs */

#endif /* BS_2DBPPGC_IRREGULAR_PIECES_HOMOGENEOUS_BINS_BS_HPP_ */
