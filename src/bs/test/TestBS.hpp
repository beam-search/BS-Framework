/*
 * TestBS.h
 *
 *  Created on: Jun 15, 2017
 *      Author: calegria
 */

#ifndef BS_TEST_TESTBS_HPP_
#define BS_TEST_TESTBS_HPP_

#include "TestBSNode.hpp"

#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

namespace bs
{

  //
  //
  //

  class TestBS
  {
  public:
    typedef TestBSNode node_type;

    TestBS (size_t fw = 2, size_t bw = 3) :
	filter_width (fw), beam_width (bw)
    {
      srand (time (0));
    }

    virtual
    ~TestBS ()
    {
    }

    size_t
    getFilterWidth () const
    {
      return filter_width;
    }

    size_t
    getBeamWidth () const
    {
      return beam_width;
    }

    template<class InIterator, class OutIterator>
      void
      filter_nodes (InIterator in_it_begin, InIterator in_it_end,
		    OutIterator fit_ini, OutIterator fit_end)
      {
	cout << "On TestBS.hpp - input" << endl;
	for (; in_it_begin != in_it_end; ++in_it_begin)
	  {
	    TestBSNode *node = *in_it_begin;
	    cout << "Property One = " << node->getPropertyOne () << endl;
	    cout << "Property Two = " << node->getPropertyTwo () << endl;
	    cout << endl;
	  }
	cout << string (10, '-') << endl;

	cout << "On TestBS.hpp - output" << endl;
	for (; fit_ini != fit_end; ++fit_ini)
	  {
	    TestBSNode *node = new TestBSNode (std::rand (), std::rand ());
	    cout << "Property One = " << node->getPropertyOne () << endl;
	    cout << "Property Two = " << node->getPropertyTwo () << endl;
	    cout << endl;

	    (*fit_ini) = node;
	  }
	cout << string (10, '-') << endl;
      }

    bool
    stop ()
    {
      return true;
    }

  private:
    const size_t filter_width;
    const size_t beam_width;
  };

} /* namespace bs */

#endif /* BS_TEST_TESTBS_HPP_ */
